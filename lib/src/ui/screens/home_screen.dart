import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_pull_refresh/src/core/blocs/home_bloc.dart';
import 'package:flutter_pull_refresh/src/core/events/home_event.dart';
import 'package:flutter_pull_refresh/src/core/states/home_state.dart';
import 'package:flutter_pull_refresh/src/ui/widgets/item_photo.dart';
import 'package:flutter_pull_refresh/src/ui/widgets/refresh_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = 'home';

  const HomeScreen({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> {
  HomeBloc? _bloc;
  final RefreshController _refreshController = RefreshController();

  void _onRefreshView() {
    debugPrint('HomeScreen # on refresh');
    _bloc?.add(HomeEventInit(isRefresh: true));
    _refreshController.refreshCompleted();
  }

  void _onLoadMore() {
    debugPrint('HomeScreen # on load more');
    _bloc?.add(HomeEventInit(isRefresh: false));
  }

  @override
  void initState() {
    super.initState();
    _bloc = context.read<HomeBloc>()..add(HomeEventInit(isRefresh: true));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pull Refresh Infinite Scroll'),
      ),
      body: SafeArea(
        child: _buildContent(),
      ),
    );
  }

  Widget _buildContent() {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (BuildContext context, HomeState state) {
        if (_refreshController.isRefresh) {
          _refreshController.refreshCompleted();
        } else if (_refreshController.isLoading) {
          _refreshController.loadComplete();
        }
        return RefreshView(
          refreshController: _refreshController,
          onRefresh: _onRefreshView,
          onLoadMore: _onLoadMore,
          child: state.isLoading
              ? _loadingView()
              : state.photos.isEmpty
                  ? _emptyView()
                  : _dataView(state),
        );
      },
    );
  }

  Widget _dataView(HomeState state) {
    return GridView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: state.photos.length,
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 1 / 1,
      ),
      physics: const ScrollPhysics(),
      itemBuilder: (context, index) {
        return ItemPhoto(imageUrl: state.photos[index].url ?? '');
      },
    );
  }
}

Widget _loadingView() {
  return const Center(
    child: CircularProgressIndicator(),
  );
}

Widget _emptyView() {
  return const Center(
    child: Text(
      'No Data Found',
      style: TextStyle(
        fontSize: 30,
        fontWeight: FontWeight.bold,
      ),
    ),
  );
}
