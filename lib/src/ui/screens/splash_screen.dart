import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_pull_refresh/src/ui/screens/home_screen.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  final Duration duration = const Duration(seconds: 3);

  @override
  Widget build(BuildContext context) {
    Timer(duration, () {
      Navigator.pushNamedAndRemoveUntil(context, HomeScreen.routeName, ModalRoute.withName("/"));
    });
    return const Scaffold(
      body: Center(
        child: Text(
          'Pull Refresh',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
