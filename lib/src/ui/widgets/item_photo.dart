import 'package:flutter/material.dart';

class ItemPhoto extends StatelessWidget {
  final String imageUrl;

  const ItemPhoto({Key? key, required this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
      imageUrl,
      fit: BoxFit.cover,
    );
  }
}
