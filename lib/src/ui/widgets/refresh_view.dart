import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

const normalStyle = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.bold,
);
const failedStyle = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.bold,
  color: Colors.red,
);

class RefreshView extends StatefulWidget {
  const RefreshView({
    Key? key,
    required this.refreshController,
    required this.child,
    this.onRefresh,
    this.onLoadMore,
  }) : super(key: key);

  final RefreshController refreshController;
  final Widget child;
  final VoidCallback? onRefresh;
  final VoidCallback? onLoadMore;

  @override
  State<StatefulWidget> createState() {
    return _RefreshViewState();
  }
}

class _RefreshViewState extends State<RefreshView> {
  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      controller: widget.refreshController,
      onRefresh: widget.onRefresh,
      onLoading: widget.onLoadMore,
      child: widget.child,
      enablePullDown: true,
      enablePullUp: true,
      footer: _buildFooterRefresh(),
    );
  }

  CustomFooter _buildFooterRefresh() {
    return CustomFooter(
      loadStyle: LoadStyle.ShowWhenLoading,
      builder: (context, mode) {
        Widget body;
        if (mode == LoadStatus.loading) {
           body = const CircularProgressIndicator();
        } else if (mode == LoadStatus.canLoading) {
          body = const Text('Load More', style: normalStyle);
        } else if (mode == LoadStatus.idle) {
          body = const Text('Success', style: normalStyle);
        } else if (mode == LoadStatus.failed) {
          body = const Text('Failed', style: failedStyle);
        } else {
          body = const Text('No More Data', style: normalStyle);
        }
        return SizedBox(
          height: 60,
          child: Center(
            child: body,
          ),
        );
      },
    );
  }
}
