import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_pull_refresh/src/core/blocs/home_bloc.dart';
import 'package:flutter_pull_refresh/src/ui/screens/home_screen.dart';
import 'package:flutter_pull_refresh/src/ui/screens/splash_screen.dart';

class AppRoute {
  static Route<dynamic> routes(RouteSettings settings) {
    if (settings.name == '/') {
      return MaterialPageRoute(builder: (context) {
        return const SplashScreen();
      });
    } else if (settings.name == HomeScreen.routeName) {
      return MaterialPageRoute(builder: (context) {
        return BlocProvider(
          create: (context) => HomeBloc(),
          child: const HomeScreen(),
        );
      });
    } else {
      return MaterialPageRoute(builder: (context) {
        return const Scaffold(
          body: Center(
            child: Text('Page Not Found'),
          ),
        );
      });
    }
  }
}
