import 'package:equatable/equatable.dart';

abstract class HomeEvent extends Equatable{}

class HomeEventInit extends HomeEvent {
  final bool isRefresh;
  HomeEventInit({required this.isRefresh});
  @override
  List<Object?> get props => [isRefresh];
}