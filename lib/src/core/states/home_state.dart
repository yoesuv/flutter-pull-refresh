import 'package:equatable/equatable.dart';
import 'package:flutter_pull_refresh/src/core/models/photos_response.dart';

class HomeState extends Equatable {
  final List<Photos> photos;
  final bool isLoading;
  final int page;

  const HomeState({
    this.photos = const [],
    this.isLoading = true,
    this.page = 1,
  });

  HomeState copyWith({List<Photos>? photos, bool? isLoading, int? page}) => HomeState(
        photos: photos ?? this.photos,
        isLoading: isLoading ?? this.isLoading,
        page: page ?? this.page,
      );

  @override
  List<Object?> get props => [photos, isLoading, page];
}
