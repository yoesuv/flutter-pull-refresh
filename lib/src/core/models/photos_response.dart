import 'package:equatable/equatable.dart';

class Photos extends Equatable{
  
  Photos({
    this.albumId,
    this.id,
    this.title,
    this.url,
    this.thumbnailUrl,
  });

  factory Photos.fromJson(Map<String, dynamic> json) => Photos(
    albumId: json['albumId'],
    id: json['id'],
    title: json['title'],
    url: json['url'],
    thumbnailUrl: json['thumbnailUrl'],
  );

  static List<Photos> buildListFromJson(List<dynamic> json) {
    return json.map((dynamic i) => Photos.fromJson(i)).toList();
  }

  int? albumId;
  int? id;
  String? title;
  String? url;
  String? thumbnailUrl;

  Map<String, dynamic> toJson() => {
    'albumId': albumId,
    'id': id,
    'title': title,
    'url': url,
    'thumbnailUrl': thumbnailUrl,
  };

  @override
  List<Object> get props => <Object>[id ?? 0, title ?? '', url ?? '', thumbnailUrl ?? ''];

}