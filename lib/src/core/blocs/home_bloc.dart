import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_pull_refresh/src/core/events/home_event.dart';
import 'package:flutter_pull_refresh/src/core/repositories/app_repository.dart';
import 'package:flutter_pull_refresh/src/core/states/home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {

  final repoApp = AppRepository();

  HomeBloc() : super(const HomeState()){
    on<HomeEventInit>(_getPhotos);
  }

  void _getPhotos(HomeEventInit event, Emitter<HomeState> emit) async {
    final isLoading = event.isRefresh ? true : false;
    final currentPage = event.isRefresh ? 1 : state.page + 1;
    debugPrint('HomeBloc # _getPhotos event is refresh ${event.isRefresh} page $currentPage');
    emit(state.copyWith(isLoading: isLoading, page: currentPage));
    try {
      final result = await repoApp.getListPost(currentPage);
      debugPrint('HomeBloc # data length ${result.length}');
      emit(state.copyWith(
        isLoading: false,
        photos: currentPage > 1 ? state.photos + result : result,
        page: currentPage,
      ));
    } catch (error) {
      debugPrint('HomeBloc # error $error');
    }
  }

}