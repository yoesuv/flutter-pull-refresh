import 'package:dio/dio.dart';
import 'package:flutter_pull_refresh/src/core/data/constants.dart';
import 'package:flutter_pull_refresh/src/core/models/photos_response.dart';
import 'package:flutter_pull_refresh/src/core/networks/network_helper.dart';

class AppRepository{

  final NetworkHelper _networkHelper = NetworkHelper();

  Future<List<Photos>> getListPost(int start) async {
    final Response<dynamic> response = await _networkHelper.get('photos?_start=$start&_limit=$POST_LIMIT') as Response<dynamic>;
    return Photos.buildListFromJson(response.data as List<dynamic>);
  }
}