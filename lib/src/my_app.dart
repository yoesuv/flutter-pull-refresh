import 'package:flutter/material.dart';
import 'package:flutter_pull_refresh/src/core/route/app_route.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Pull Refresh',
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      onGenerateRoute: AppRoute.routes,
    );
  }
}
