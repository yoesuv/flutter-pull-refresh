## Flutter Pull Refresh ##

A Flutter project implement function Pull to Refresh and Infinite Scroll.

#### List Library ####
- [Bloc Library](https://pub.dev/packages/dio)
- [Dio](https://pub.dev/packages/dio)
- [Equatable](https://pub.dev/packages/equatable)
- [Provider](https://pub.dev/packages/provider)
- [Pull to Refresh](https://pub.dev/packages/pull_to_refresh)
